<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div>
</div>

	<footer id="colophon" class="site-footer" role="contentinfo">
	</footer><!-- .site-footer -->

<?php wp_footer(); ?>
<script src="/wp-content/themes/antoinette/menu.js"></script>

</body>
</html>
