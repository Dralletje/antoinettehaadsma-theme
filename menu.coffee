# Global listeners
blur = false
window.addEventListener 'blur', ->
  console.log("Blurred")
  blur = true
window.addEventListener 'focus', ->
  console.log("Not blurred")
  blur = false

cursor = [0,0]
document.addEventListener 'mousemove', (e) ->
    cursor[0] = e.pageX
    cursor[1] = e.pageY

width = window.innerWidth or document.documentElement.clientWidth or document.body.clientWidth;

# Background component
Background = React.createClass
  displayName: 'Slider'

  getInitialState: ->
    background: @props.initialBackground
    previousBackground: @props.initialBackground + 1

  style: (i) ->
    background = @props.backgrounds[i]
    background: "url('#{background.url}') no-repeat center center fixed"

  componentDidMount: ->
    # Preload images
    backgrounds = @props.backgrounds.slice()
    loadNext = ->
      background = backgrounds.shift()
      if not background? then return
      img = new Image
      img.onload = ->
          loadNext()
      img.src = background.url
    loadNext()

    this.interval = setInterval(@changeImage, 7000)

  componentWillUnmount: ->
    clearInterval(this.interval)

  changeImage: ->
    if blur
      return console.log("Not changing the image :)")

    i = @state.background
    if @props.backgrounds.length <= i+1
      @setState
        previousBackground: i
        background: 0
      sessionStorage.setItem('background-index', 0)
    else
      @setState
        previousBackground: i
        background: i+1
      sessionStorage.setItem('background-index', i+1)

  render: ->
    <div>
	    <div className="current-background background"
           style={@style(@state.background)}
           key={@state.background} />
	    <div className="prev-background background"
           style={@style(@state.previousBackground)}
           key={@state.previousBackground} />
    </div>


shuffleArray = (array) ->
  for _,i in array
      j = Math.floor(Math.random() * (i + 1))
      temp = array[i]
      array[i] = array[j]
      array[j] = temp
  return array

# Load order from sessionStorage
N = 25
loaded = sessionStorage.getItem('order')
if loaded?
then order = JSON.parse('[' + loaded + ']')
else order = shuffleArray(Array.apply(null, length: N).map(Number.call, Number))
sessionStorage.setItem('order', order);


# Change order into urls
pictures = order
           .map (x) -> "/wp-content/themes/antoinette/background/" + x.toString() + '.jpg'
           .map (url) -> url: url

# Render background
i = sessionStorage.getItem('background-index')
i = if i? then parseInt(i) else 0
React.render(
  <Background initialBackground={i} backgrounds={pictures} />,
  document.getElementById('backgroundthing')
)


Menu = React.createClass
  displayName: 'Menu'

  getInitialState: ->
    open: false
  open: ->
    clearTimeout(this.currentTimeout) # Just for sure
    @setState open: true
    document.body.className = document.body.className + ' blur'
    setTimeout -> # Haskish
      document.body.className = document.body.className + ' blur'
    , 40
  close: ->
    clearTimeout(this.currentTimeout) # Just for sure
    @setState open: false
    document.body.className = document.body.className.split(' ').filter((x) => x isnt 'blur').join(" ")

  makeReadyToClose: ->
    this.check(0, 10000, 0)

  check: (topSlope, bottomSlope, i) ->
    if i > 3 # Three fails may occur before close
      return this.close()

    {offsetTop, offsetHeight, offsetWidth} = this.refs.container.getDOMNode()
    p1 = [offsetWidth, offsetTop-100]
    p2 = [offsetWidth, offsetTop+offsetHeight+100]

    currentTopSlope = (p1[1] - cursor[1]) / (p1[0] - cursor[0])
    currentBottomSlope = (p2[1] - cursor[1]) / (p2[0] - cursor[0])

    if not (currentTopSlope <= topSlope and currentBottomSlope >= bottomSlope)
      i = i + 1

    clearTimeout(this.currentTimeout) # Just for sure
    this.currentTimeout = setTimeout =>
      this.check(currentTopSlope, currentBottomSlope, i)
    , 10

  render: ->
    items = @props.items.map ([title, url], i) ->
      <li key={i}><a href={url}>{title}</a></li>

    onTotalDiv = undefined
    onJustLink = undefined

    if @state.open
    then onTotalDiv = @open
    else onJustLink = @open

    <div onMouseEnter={onTotalDiv} onMouseLeave={@makeReadyToClose} ref="container">
      <a onMouseEnter={onJustLink} href={@props.href}>{@props.children}</a>
      <ul className={"deeper " + (if @state.open then "" else "closed")}>
        {items}
      </ul>
    </div>



# Render them
items = [
  ['Miinu', '/miinu']
]
React.render(
  <Menu href="#" items={items}>Dealers</Menu>,
  document.getElementById("link-dealers")
)

# Merken
merken = [
  #['Andreu World', '/andreu']
  # ['Havee meubelen', '/havee']
  ['Miinu', '/brand-miinu']
  ['Verotex', '/verotex']
  ['Vondom', '/vondom']
  ['Andreu World', '/andreu']
]
React.render(
  <Menu href="#" items={merken}>Brands</Menu>,
  document.getElementById("link-brands")
)
