<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" media="screen" href="/wp-content/themes/antoinette/style.css" />

	<script src="//cdnjs.cloudflare.com/ajax/libs/react/0.13.2/react-with-addons.js"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div id="backgroundthing"></div>

	<?php
	$items = open_menu();
	?>

	<div class="site-title">
		<a href="/">
			<span>Antoinette</span>
			<b>Haadsma</b>
			<span>Collections</span>
		</a>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="nav">
				<ul>
					<?php foreach($items as $item): ?>
						<?php preg_match("/\/([a-z0-9-]+)\//", $item->url, $matches); ?>
						<?php if($item->title === "") $item->title = ucfirst($matches[1]); ?>
						<li id="link-<?= $matches[1]; ?>">
							<a href="<?= $item->url ?>">
								<?= $item->title ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
