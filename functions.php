<?php

register_nav_menu('sidebar', 'The menu on the sidebar :-D');

function open_menu() {
  $menu = wp_get_nav_menus();
  return array_map(function($item) {
    return (object) array(
      'title' => $item->post_title,
      'url' => $item->url
    );
  }, wp_get_nav_menu_items($menu[0]->slug));
}

?>
